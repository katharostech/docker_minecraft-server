# Docker Minecraft Server #
This is a *simple* Minecraft server. It will run the official Minecraft server jar and allow you to set any of the server.properties settings through environment variables. It is running Java 8 on top of Ubuntu.
